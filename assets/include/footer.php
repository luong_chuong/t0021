<!--***** footer *****-->
<footer class="c-footer">

	<div class="c-footer__1">
		<div class="l-container">
			<div class="c-footer__block">
				<div class="c-footer__title">株式会社みずほ銀行</div>
				<div class="c-footer__text">
					<p>登録金融機関 関東財務局長（登金） 第6号<br>
					加入協会：日本証券業協会 一般社団法人金融先物取引業協会<br>
					一般社団法人第二種金融商品取引業協会</p>
				</div>
				<div class="c-footer__list">
					<ul class="c-list1">
						<li><a href="">金融商品勧誘方針</a></li>
						<li><a href="">個人情報のお取扱いについて</a></li>
						<li><a href="">法人のお客さま情報の共有について</a></li>
						<li><a href="">特定投資家制度における期限日について</a></li>
						<li><a href="">当行のマネー・ローンダリング等防止態勢</a></li>
						<li><a href="">利益相反管理方針の概要</a></li>
					</ul>
				</div>
			</div>
			<div class="c-footer__block">
				<div class="c-footer__title">株式会社みずほ銀行</div>
				<div class="c-footer__list">
					<ul class="c-list1">
						<li><a href="">みずほフィナンシャルグループ</a></li>
						<li><a href="">みずほ銀行</a></li>
						<li><a href="">みずほ信託銀行</a></li>
						<li><a href="">みずほ証券</a></li>
						<li><a href="">みずほ総合研究所</a></li>
						<li><a href="">みずほ情報総研</a></li>
						<li><a href="">資産管理サービス信託銀行</a></li>
						<li><a href="">みずほ投信投資顧問</a></li>
						<li><a href="">DIAMアセットマネジメント</a></li>
					</ul>
				</div>
			</div>
			<div class="c-footer__block">
				<div class="c-footer__text">
					<p>アニメーションをご覧いただくには、アドビシステムズ社が配布しているAdobe Flash Player（無償）が必要です。</p>
					<img src="assets/image/common/icon-adobe1.png" alt="" width="110" height="30">
				</div>
				<div class="c-footer__text">
					<p>PDFファイルをご覧いただくには、アドビシステムズ社が配布しているAdobe Reader（無償）が必要です。</p>
					<img src="assets/image/common/icon-adobe2.png" alt="" width="110" height="31">
				</div>
			</div>
		</div>
	</div>

	<div class="c-footer__2">
		<div class="l-container">
			<p>Copyright (C) 2015 Mizuho Bank,Ltd. All Rights Reserved.</p>
		</div>
	</div>
</footer>

<script src="/assets/js/functions.js"></script>
</body>
</html>