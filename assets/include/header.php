<!DOCTYPE html>
<html lang="ja" id="pagetop">
<head>
<meta charset="UTF-8">
<meta name="format-detection" content="telephone=no">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
<?php include($_SERVER['DOCUMENT_ROOT'] . '/assets/include/meta.php'); ?>
<link href="/assets/css/style.css" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Noto+Sans" rel="stylesheet">
<script src="/assets/js/jquery-2.2.4.min.js"></script>
<script src="https://use.fontawesome.com/c92d851ab9.js"></script>

</head>
<body class="page-<?php echo $id; ?>">
	<!--***** header *****-->
	<header class="c-header">
		<!-- top navigator -->
		<div class="c-header__top">
			<div class="l-container">
				<div class="c-header__logo">
					<h1><a href=""><img src="assets/image/common/logo_web.png" alt="" width="220" height="52"></a></h1>
				</div>
				<div class="c-header__right">
					<div class="c-header__list">
						<ul class="c-list1 c-list1--inline">
							<li><a href=""><strong>資料請求</strong></a></li>
							<li><a href=""><strong>FAQ</strong></a></li>
							<li><a href=""><strong>お問い合わせ</strong></a></li>
						</ul>
					</div>
					<div class="c-header__button">
						<div class="c-btn1">
							<a href=""><strong><i class="fa fa-sign-out" aria-hidden="true"></i>ログアウト</strong></a>
						</div>
					</div>
					<div class="c-header__image">
						<img src="assets/image/common/logo.png" alt="" width="234" height="56">
					</div>
				</div>
			</div>
		</div>
		<!-- navigator -->
		<nav class="c-header__nav">
			<ul class="l-container">
				<li><a href="">HOME</a></li>
				<li><a href="">メンバーズ会員特典</a></li>
				<li><a href="">キャンペーン一覧</a></li>
				<li><a href="">かりる</a></li>
				<li><a href="/tameru.php">ためる</a></li>
				<li><a href="">ふやす</a></li>
				<li><a href="">特集</a></li>
			</ul>
		</nav>
	</header>